# Quelques ressources pour aller plus loin

## Articles

- [SOLID](https://fr.wikipedia.org/wiki/SOLID_(informatique))

## Site Web

- [Refactoring Guru](https://refactoring.guru/fr) : un site avec une section dédiée aux Design Patterns et une autre section (en anglais) sur le code (refactoring,code smells, clean code, etc)

## Livres

- [Clean Code](https://www.amazon.fr/Clean-Code-Handbook-Software-Craftsmanship/dp/0132350882/) de Robert Martin
- [TDD By Example](https://www.amazon.fr/Test-Driven-Development-Kent-Beck/dp/0321146530) de Kent Beck
- [Refactoring](https://www.amazon.fr/Refactoring-Improving-Design-Existing-Code/dp/0134757599) de Martin Fowler
- [Working Effectively With Legacy Code](https://www.amazon.fr/Working-Effectively-Legacy-Michael-Feathers/dp/0131177052) de Micheal Feathers
- [Pragmatic Programmer](https://www.amazon.fr/Pragmatic-Programmer-journey-mastery-Anniversary/dp/0135957052) de David Thomas et Andrew Hunt

## Podcasts

- [Artisan Développeur](https://artisandeveloppeur.fr/podcast/) : un podcast plein d'interviews de passionnés qui viennent parler d'agilité, de bonnes pratiques ou qui nous font prendre un peu de recul sur notre métier.

## Vidéos

- [TDD: Au delà de l'intro (fr)](https://www.youtube.com/watch?v=RlkgetzDenI): Un excellent talk sur le TDD par Romeu Moura à Alpes Craft  
- [Trop de mock tue le test : ce que l'archi hexagonale a changé (fr)](https://www.youtube.com/watch?v=rSO1y3lCBfk): Un excellent talk qui vient déconstruire les idées reçues sur les tests unitaires

## Personnalités

- [Robert Martin, alias "Uncle Bob"](https://blog.cleancoder.com/)
- [Kent Beck](https://en.wikipedia.org/wiki/Kent_Beck)
- [Martin Fowler](https://martinfowler.com/)
